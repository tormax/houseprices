### House Prices: Advanced Regression Techniques

Predict sales prices and practice feature engineering, RFs, and gradient boosting

[Pleasing review in jupyter notebook](https://lyricslee.github.io/houseprices/kaggleHousePrices.html)

1. **Competition in Kaggle**

    https://www.kaggle.com/c/house-prices-advanced-regression-techniques
    

2. **Competition Description**

    Ask a home buyer to describe their dream house, and they probably won't begin with the height of the basement ceiling or the proximity to an east-west railroad. But this playground competition's dataset proves that much more influences price negotiations than the number of bedrooms or a white-picket fence. With 79 explanatory variables describing (almost) every aspect of residential homes in Ames, Iowa, this competition challenges you to predict the final price of each home.
    
    
3. **Project Steps:**
    
    - Load data from files
    - Preprocess data 
    - Extract features
    - Train and Predict
    - Submission
    
   
4. **Algorithm**

    Finally, we choose Huber regression Algorithm because it is robust to corrupt data.
